# Wave Payroll

by Bronislaw (Bronek) Szulc

bronek@fastmail.fm

August 2018

---

![App preview](app-preview.png)


## The tech stack

- React with React-Router
- Node.js (version 8.11.3) with Express
- Sequelize.js ORM
- Postgres database (version 10.4)

## Instructions on how to run it

### Installing App dependencies

1. let's install via npm

  From the root directory (for the api)
  > npm i

  And from the front-end app directory (for the app)
  > cd app && npm i && cd ..

### Setting up environment variables config

1. duplicate the file `.env-sample` in root and name it `.env`. Keep the contents the same.

### Setting up the database

1. Install Postgres on your Mac/Linux computer

  On Mac in the terminal
  > brew install postgresql (10.4)


2. Get Postgres running by running the following in terminal
  > pg_ctl -D /usr/local/var/postgres start

  And then
  > brew services start postgresql

  (if you would like to stop these services simply run them with "stop" instead of "start")

3. Once Postgres is running, let's create the development database and default role

  Run the following in terminal
  > psql postgres;

  Once you're int the postgres shell, run the following SQL commands
  > CREATE DATABASE development;

  > CREATE ROLE admin WITH LOGIN PASSWORD 'rollingstone';

  And then exit
  > \q

4. Now that the database is created let's create the tables and seed some initial development data

  Please ensure that you are in the root directory of this `wave-payroll` codebase. Run the following in the terminal to create the tables
  > node node_modules/.bin/sequelize db:migrate

  And the following to seed with data (e.g. job groups)
  > node node_modules/.bin/sequelize db:seed:all

Awesome! If all went well we should be able to run our api and app.

Let's take a second to wonder what it's going to look like!

Maybe grab a quick coffee and cookie?

Ok

Ok

Time to get to business

Yes?

### Running the API

From the root directory run the following
> npm start

### Running the app

In a new terminal tab and from the `/app` directory run the following
> cd app && npm start

### Viewing the app

In the browser you can visit `http://localhost:3000` to view the UI and hit `http://localhost:3001` or `http://localhost:3001/v0/time-reports`, for example, to view the REST API.

Enjoy!


---

## What I did well (IMO)

I enjoyed the project quite a lot. I think I did well in terms of organizing the back-end and front-end code respectively in such a way that it would be easy to add new features, write unit and integration tests and simply to reason about moving forward as a developer or team of developers. The DB schema and tables are intentionally as simple as I could make them. I tried to use best practice with my react code and separate data-fetching from presentation. Similarly, with the migrations, seeds and env configuration, the api and app should be relatively easy to get running for another developer starting out.

I kept in mind extensibility - as in where this app or its features could lead in the future. For example viewing reports for a specific employee id or within a pay period range.

I'm also happy about the API response being generic and not too informed by the UI needs (e.g. `v0/payroll-report` responds with "start" and "end" date for period instead of a `start / date` string. This is just raw data that's easier to deal with and the front-end can deal with the presentation and date formatting).

Overall I'm happy with my effort and if I had more time I would add unit tests with Jest and have a little more fun adding a dashboard plus simple table filters and sorting. As well as possibly separating `app` into its own repo.

Thank you very much for reviewing. I appreciate it.
