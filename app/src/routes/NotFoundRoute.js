import React from 'react';

export default function NotFoundRoute () {
  return (
    <div className="Container">
      <p>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
      </p>

      <p>a wave turns in the ocean - page not found</p>

      <p>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
        <span role="img" aria-label="">🌊</span>
      </p>
    </div>
  );
}
