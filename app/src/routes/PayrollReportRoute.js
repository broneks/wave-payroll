import React from 'react';

import { Link } from 'react-router-dom';

import PayrollReportTableContainer from '../containers/PayrollReportTableContainer';

export default function PayrollReportRoute () {
  return (
    <div className="Container">
      <h2>Payroll Report</h2>

      <p>Here is a payroll report generated from all of the previously uploaded time reports in our system. <Link to="/">Click here to view a list of which time report have been uploaded</Link>.</p>

      <PayrollReportTableContainer></PayrollReportTableContainer>
    </div>
  );
}
