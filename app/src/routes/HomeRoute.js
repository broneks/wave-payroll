import React from 'react';
import moment from 'moment';

import { Link } from 'react-router-dom';

import TimeReportListContainer from '../containers/TimeReportListContainer';

function getGreeting () {
  const afternoon = 12;
  const evening = 17;
  const currentHour = parseFloat(moment().format('HH'));

  if (currentHour >= afternoon && currentHour <= evening) {
    return 'afternoon';
  } else if (currentHour >= evening) {
    return 'evening';
  }

  return 'morning';
}

export default function TimeReportUpload () {
  return (
    <div className="Container">
      <h2>Good {getGreeting()}. Today is {moment().format('MMMM Do YYYY')}.</h2>

      <p>Below are your previously uploaded reports. <Link to="/time-report-upload">Click here to upload another time report</Link>.</p>

      <TimeReportListContainer></TimeReportListContainer>
    </div>
  );
}
