import React from 'react';

import { Link } from 'react-router-dom';

import TimeReportUploadContainer from '../containers/TimeReportUploadContainer';

export default function TimeReportUploadRoute () {
  return (
    <div className="Container">
      <h2>Time Report</h2>

      <p>Upload a time report to <Link to="/payroll-report">view a calculated payroll report</Link>. Time reports are CSV files listing employee information, including hours worked per date.</p>

      <p>A valid time report has <strong>date</strong>, <strong>hours worked</strong>, <strong>employee id</strong>, and <strong>job group</strong> as columns, as well as a final row containing <strong>a unique report id</strong>.</p>

      <TimeReportUploadContainer></TimeReportUploadContainer>
    </div>
  );
}
