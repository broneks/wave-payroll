import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from 'react-router-dom';

import './App.css';

import HomeRoute from './routes/HomeRoute';
import TimeReportUploadRoute from './routes/TimeReportUploadRoute';
import PayrollReportRoute from './routes/PayrollReportRoute';
import NotFoundRoute from './routes/NotFoundRoute';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-Sidebar">
            <div className="App-Logo">
              <h1>Wave Payroll</h1>
            </div>

            <nav className="App-Nav">
              <ul className="App-Nav__list">
                <li className="App-Nav__list-item">
                  <NavLink exact={true} to="/" activeClassName='is-active'>Dashboard</NavLink>
                </li>

                <li className="App-Nav__list-item">
                  <NavLink to="/time-report-upload" activeClassName='is-active'>Upload a time report</NavLink>
                </li>

                <li className="App-Nav__list-item">
                  <NavLink to="/payroll-report" activeClassName='is-active'>View payroll report</NavLink>
                </li>
              </ul>
            </nav>

            <div className="Made-By">2018 - Made by Bronek Szulc</div>
          </header>

          <div className="App-Body">
            <Switch>
              <Route exact path="/" component={HomeRoute}></Route>
              <Route exact path="/time-report-upload" component={TimeReportUploadRoute}></Route>
              <Route exact path="/payroll-report" component={PayrollReportRoute}></Route>
              <Route component={NotFoundRoute} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
