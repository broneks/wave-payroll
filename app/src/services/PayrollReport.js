import Resource from './Resource';

export default class PayrollReport extends Resource {
  constructor () {
    super({
      endpoint: 'payroll-report',
    })
  }

  get () {
    return super.get()
      .then(({ data }) => ({
        report: data,
        columns: Object.keys(data[0]) || [],
      }));
  }
}
