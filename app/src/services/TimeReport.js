import Resource from './Resource';
import axios from 'axios';

export default class TimeReport extends Resource {
  constructor () {
    super({
      endpoint: 'time-reports',
    })
  }

  upload (file) {
    if (file == null) return Promise.reject('no file to upload.');

    const formData = new FormData();
    formData.append('timeReport', file);

    return axios.post(`${this.url}/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  }
}
