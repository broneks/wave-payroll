import axios from 'axios';

class Resource {
  constructor ({
    apiBaseUrl = 'http://localhost:3001/v0/',
    endpoint = '',
  }) {
    this.url = `${apiBaseUrl}${endpoint}`;
  }

  get () {
    return axios.get(this.url)
      .then(res => ({
        res,
        data: res.data,
      }))
  }
}

export default Resource;
