import React, { Component } from 'react';
import moment from 'moment';

import TimeReport from '../services/TimeReport';
import Table from '../components/Table/Table';

const timeReport = new TimeReport();

export default class TimeReportListContainer extends Component {
  constructor () {
    super();

    this.state = {
      loading: true,
      reports: [],
      columns: this.getColumnsMap(),
    };
  }

  componentDidMount () {
    timeReport.get()
      .then((res) => {
        this.setState({
          loading: false,
          reports: res.data || [],
        });
      })
      .catch(() => {
        this.setState({
          loading: false,
        });
      });
  }

  getColumnsMap () {
    const dateFormat = 'D/M/YYYY';

    return [
      {
        title: 'Report Id',
        key: 'reportId',
      },
      {
        title: 'Created',
        key: 'createdAt',
        getter: row => moment(row.createdAt).format(dateFormat),
      },
    ];
  }

  render () {
    const { loading, columns, reports } = this.state;

    return (
      <div>
        <Table
          loading={loading}
          columns={columns}
          rows={reports}></Table>
      </div>
    );
  }
}
