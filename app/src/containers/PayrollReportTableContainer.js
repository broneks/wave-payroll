import React, { Component } from 'react';
import moment from 'moment';

import PayrollReport from '../services/PayrollReport';
import Table from '../components/Table/Table';

const payrollReport = new PayrollReport();

export default class PayrollReportTableContainer extends Component {
  constructor () {
    super();

    this.state = {
      loading: true,
      report: [],
      columns: this.getColumnsMap(),
    };
  }

  componentDidMount () {
    payrollReport.get()
      .then(({ report }) => {
        this.setState({
          loading: false,
          report,
        });
      })
      .catch(() => {
        this.setState({
          loading: false,
        });
      });
  }

  getColumnsMap () {
    const dateFormat = 'D/M/YYYY';

    return [
      {
        title: 'Employee Id',
        key: 'employeeId',
      },
      {
        title: 'Pay Period',
        key: 'payPeriod',
        getter: row => `${moment(row.start).format(dateFormat)} - ${moment(row.end).format(dateFormat)}`,
      },
      {
        title: 'Amount Paid',
        key: 'pay',
        getter: row => `$ ${row.pay.toFixed(2)}`,
      },
    ];
  }

  render () {
    const {
      loading,
      columns,
      report,
    } = this.state;

    return (
      <Table
        loading={loading}
        columns={columns}
        rows={report}></Table>
    );
  }
}
