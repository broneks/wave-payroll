import React, { Component } from 'react';

import { withRouter } from "react-router-dom";

import TimeReport from '../services/TimeReport';
import FileUploader from '../components/FileUploader/FileUploader';
import Notification from '../components/Notification/Notification';

const timeReport = new TimeReport();

class TimeReportUploadContainer extends Component {
  constructor () {
    super();

    this.state = {
      status: null,
      message: null,
    };
  }

  uploadFile (files = []) {
    const file = files[0];

    if (file == null) {
      this.setState({
        status: 'error',
        message: `That file is invalid or not a CSV. Try a different file.`,
      });
      return;
    }

    timeReport.upload(file)
      .then(() => {
        this.setState({
          status: 'success',
          message: `the time report "${file.name}" was successfully uploaded! You will be redirected to the payroll report momentarily.`,
        }, () => {
          window.setTimeout(() => {
            this.props.history.push('/payroll-report');
          }, 3000);
        });
      })
      .catch((res) => {
        this.setState({
          status: 'error',
          message: `${res.response.data.error}. Try a different file.`,
        });
      });
  }

  render () {
    const { status, message } = this.state;

    let notification;
    if (status) {
      notification = (
        <Notification type={status} message={message}></Notification>
      );
    }

    return (
      <div>
        {notification}

        <FileUploader onDrop={(...args) => this.uploadFile(...args)}></FileUploader>
      </div>
    );
  }
}

export default withRouter(TimeReportUploadContainer);
