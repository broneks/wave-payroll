import React from 'react';
import { Link } from "react-router-dom";
import { AutoSizer, Table, Column } from 'react-virtualized';

import 'react-virtualized/styles.css';

import './Table.css';

function NoRows ({ loading }) {
  if (loading) {
    return (
      <div className="Table-Loading">Loading...</div>
    );
  }

  return (
    <div className="Table-No-Rows">No rows... <Link to="/time-report-upload">Try uploading a time report</Link> to get started.</div>
  );
}

export default function TableDisplay ({
  loading,
  columns,
  rows,
}) {
  const columnNodes = columns.map(({ title, key, getter }) => (
    <Column
      key={key}
      className="Table-Cell"
      headerClassName="Table-Column-Header"
      width={300}
      label={title}
      dataKey={key}
      cellRenderer={({ cellData, rowData }) => {
        return (getter != null && typeof getter === 'function')
          ? getter(rowData)
          : cellData;
      }}
    />
  ));

  return (
    <AutoSizer disableHeight>
      {({ width }) => (
        <Table
          ref="Table"
          className="Table"
          gridClassName="Table-Grid"
          rowClassName="Table-Row"
          height={500}
          width={width}
          headerHeight={50}
          rowHeight={35}
          rowCount={rows.length}
          rowGetter={({ index }) => rows[index]}
          noRowsRenderer={() => (
            <NoRows loading={loading}></NoRows>
          )}>
          {columnNodes}
        </Table>
      )}
    </AutoSizer>
  );
}
