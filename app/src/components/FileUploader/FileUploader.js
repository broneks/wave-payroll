import React from 'react';
import Dropzone from 'react-dropzone';

import './FileUploader.css';

export default function FileUploader ({
  onDrop,
}) {
  return (
    <div className="File-Uploader">
      <Dropzone
        className="File-Uploader__dropzone"
        accept="text/csv"
        onDrop={(...args) => onDrop(...args)}>
        <p className="File-Uploader__label">Drag and drop a CSV</p>
      </Dropzone>
    </div>
  );
}
