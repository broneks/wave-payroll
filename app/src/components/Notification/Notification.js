import React from 'react';

import './Notification.css';

export default function Notification ({
  type,
  message,
}) {
  if (message == null) return null;

  let typeClassName;
  switch (type) {
    case 'error':
      typeClassName = 'Notification--error';
      break;

    case 'success':
      typeClassName = 'Notification--success';
      break;

    // no default
  }

  return (
    <div className={`Notification ${typeClassName}`}>
      {message}
    </div>
  );
}
