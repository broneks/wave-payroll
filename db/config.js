require('dotenv').config();

const Sequelize = require('sequelize');

module.exports = {
  development: {
    dialect: 'postgres',
    username: process.env.PGUSERNAME,
    password: process.env.PGPASSWORD,
    database: process.env.PGDATABASE,
    host: process.env.PGHOST,
    port: process.env.PGPORT,
    timezone: 'utc',
    operatorsAliases: Sequelize.Op,
    logging: false,
  },
  test: {
    dialect: 'postgres',
    timezone: 'utc',
    operatorsAliases: Sequelize.Op,
    logging: false,
  },
  production: {
    dialect: 'postgres',
    timezone: 'utc',
    operatorsAliases: Sequelize.Op,
  },
};
