module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('timeReportLogs', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      timeReportId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'timeReports',
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      jobGroupId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'jobGroups',
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      employeeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      hoursWorked: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      dateLogged: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('timeReportLogs');
  }
};
