module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('timeReports', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      reportId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true,
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('timeReports', { cascade: true });
  }
};
