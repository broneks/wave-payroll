module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createdAt = updatedAt = new Date();

    // job groups
    await queryInterface.bulkInsert('jobGroups', [
      { title: 'A', hourlyPay: 20, createdAt, updatedAt },
      { title: 'B', hourlyPay: 30, createdAt, updatedAt },
    ], {});

    const jobGroupA = await queryInterface.sequelize.query(
      `SELECT id FROM "jobGroups" WHERE title = 'A';`
    );
    const jobGroupB = await queryInterface.sequelize.query(
      `SELECT id FROM "jobGroups" WHERE title = 'B';`
    );

    // time reports
    await queryInterface.bulkInsert('timeReports', [
      { reportId: 1, createdAt, updatedAt },
      { reportId: 2, createdAt, updatedAt },
    ], {});

    const report1 = await queryInterface.sequelize.query(
      'SELECT id FROM "timeReports" WHERE "reportId" = 1;'
    );
    const report2 = await queryInterface.sequelize.query(
      'SELECT id FROM "timeReports" WHERE "reportId" = 2;'
    );

    // time report logs
    return queryInterface.bulkInsert('timeReportLogs', [
      { timeReportId: report1[0][0].id, jobGroupId: jobGroupA[0][0].id, employeeId: 1, hoursWorked: 5.5, dateLogged: new Date('5/1/2018'), createdAt, updatedAt },
      { timeReportId: report1[0][0].id, jobGroupId: jobGroupA[0][0].id, employeeId: 1, hoursWorked: 7.5, dateLogged: new Date('5/2/2018'), createdAt, updatedAt },
      { timeReportId: report2[0][0].id, jobGroupId: jobGroupA[0][0].id, employeeId: 1, hoursWorked: 9, dateLogged: new Date('6/12/2018'), createdAt, updatedAt },
      { timeReportId: report2[0][0].id, jobGroupId: jobGroupB[0][0].id, employeeId: 2, hoursWorked: 3, dateLogged: new Date('6/12/2018'), createdAt, updatedAt },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('timeReportLogs', null, {});
    await queryInterface.bulkDelete('timeReports', null, {});
    return queryInterface.bulkDelete('jobGroups', null, {});
  }
};
