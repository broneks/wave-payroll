const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const mountRoutes = require('./api/routes');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// const whitelist = [
//   'http://0.0.0.0:3000',
//   'http://localhost:3000',
// ];
// app.use(cors({
//   origin (origin, cb) {
//     if (whitelist.indexOf(origin) !== -1) {
//       cb(null, true)
//     } else {
//       cb(new Error('Not allowed by CORS'))
//     }
//   }
// }));
app.use(cors());

mountRoutes(app);

app.get('*', (req, res) => {
  res.status(404).send('a wave turns in the ocean - page not found.');
});

module.exports = app;
