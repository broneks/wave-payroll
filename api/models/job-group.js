module.exports = (sequelize, DataTypes) => {
  const jobGroup = sequelize.define('jobGroup', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    hourlyPay: {
      type: DataTypes.REAL,
      allowNull: false,
      validate: {
        min: 0,
      },
    },
  });

  jobGroup.associate = (models) => {
    models.jobGroup.hasMany(models.timeReportLog);
  };

  return jobGroup;
};
