module.exports = (sequelize, DataTypes) => {
  const timeReport = sequelize.define('timeReport', {
    reportId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
    },
  });

  timeReport.associate = (models) => {
    models.timeReport.hasMany(models.timeReportLog);
  };

  return timeReport;
};
