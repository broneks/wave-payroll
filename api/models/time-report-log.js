module.exports = (sequelize, DataTypes) => {
  const timeReportLog = sequelize.define('timeReportLog', {
    employeeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    hoursWorked: {
      type: DataTypes.REAL,
      allowNull: false,
    },
    dateLogged: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
  });

  timeReportLog.associate = (models) => {
    models.timeReportLog.belongsTo(models.timeReport, {
      onDelete: 'cascade',
      foreignKey: {
        allowNull: false,
      },
    });

    models.timeReportLog.belongsTo(models.jobGroup, {
      onDelete: 'cascade',
      foreignKey: {
        allowNull: false,
      },
    });
  };

  return timeReportLog;
};
