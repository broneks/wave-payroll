const moment = require('moment');

class PayrollReportGenerator {
  constructor () {
    this.reset();
  }

  reset () {
    this.report = [];
  }

  getLastDayInMonth (year, month) {
    const start = moment([year, month - 1]);
    const end = moment(start).endOf('month');

    return end.format('YYYY-MM-DD');
  }

  calculateBiMonthly (employeeTimeReportLogs) {
    const payDates = Object.keys(employeeTimeReportLogs.payByDateLogged);

    return employeeTimeReportLogs.datesLogged
      .map(date => ({
        date,
        rows: [
          {
            employeeId: employeeTimeReportLogs.employeeId,
            start: `${date}-01`,
            end: `${date}-15`,
            pay: 0,
          },
          {
            employeeId: employeeTimeReportLogs.employeeId,
            start: `${date}-16`,
            end: this.getLastDayInMonth(...date.split('-')),
            pay: 0,
          },
        ],
      }))
      .map(({ date, rows }) => (
        payDates
          .reduce((arr, payDate) => {
            const [ year, month, day ] = payDate.split('-');

            if (`${year}-${month}` === date) {
              const pay = employeeTimeReportLogs.payByDateLogged[payDate];

              if (day < 16) {
                arr[0].pay += pay;
              } else {
                arr[1].pay += pay;
              }
            }

            return arr;
          }, rows)
          .filter(arr => arr.pay > 0)
      ))
  }

  generate (logs, payFrequency = 'bimonthly') {
    this.reset();

    this.report = logs
      .map((log) => {
        switch (payFrequency) {
          case 'bimonthly':
          default:
            return this.calculateBiMonthly(log);
        }
      })
      .reduce((arr, rows) => (
        arr.concat(...rows)
      ), []);

    return this.report;
  }
}

module.exports = PayrollReportGenerator;
