const PayrollReportGenerator = require('./payroll-report-generator');

const payrollReportGenerator = new PayrollReportGenerator();

test('transforms raw employee log data into a list of payroll report items', () => {
  const cases = [
    {
      input: [
        {
          "employeeId": 1,
          "datesLogged": [
            "2018-05",
            "2018-06"
          ],
          "payByDateLogged": {
            "2018-05-01": 110,
            "2018-05-02": 150,
            "2018-06-12": 180
            }
          },
        {
          "employeeId": 2,
          "datesLogged": [
            "2018-06",
          ],
          "payByDateLogged": {
            "2018-06-12": 90
          }
        }
      ],
      output: [
        {
          "employeeId": 1,
          "start": "2018-05-01",
          "end": "2018-05-15",
          "pay": 260
        },
        {
          "employeeId": 1,
          "start": "2018-06-01",
          "end": "2018-06-15",
          "pay": 180
        },
        {
          "employeeId": 2,
          "start": "2018-06-01",
          "end": "2018-06-15",
          "pay": 90
        }
      ],
    }
  ];

  cases.forEach((c) => {
    expect(payrollReportGenerator.generate(c.input)).toEqual(c.output);
  });
});

test('filters out items where employee pay is zero', () => {
  const cases = [
    {
      input: [
        {
          "employeeId": 1,
          "datesLogged": [
            "2018-05",
          ],
          "payByDateLogged": {
            "2018-05-01": 0,
            "2018-05-02": 0,
            }
          },
        {
          "employeeId": 2,
          "datesLogged": [
            "2018-06",
          ],
          "payByDateLogged": {
            "2018-06-12": 90
          }
        }
      ],
      output: [
        {
          "employeeId": 2,
          "start": "2018-06-01",
          "end": "2018-06-15",
          "pay": 90
        }
      ],
    }
  ];
  cases.forEach((c) => {
    expect(payrollReportGenerator.generate(c.input)).toEqual(c.output);
  });
});
