const fs = require('fs');
const readLastLines = require('read-last-lines');
const csvparse = require('csv-parse');

class TimeReportParser {
  constructor ({
    filePath,
    ignoreHeaderRow = true,
    ignoreFooterRow = true,
  }) {
    this.filePath = filePath;
    this.ignoreHeaderRow = ignoreHeaderRow;
    this.ignoreFooterRow = ignoreFooterRow;
  }

  flushFile () {
    if (this.filePath == null) return;

    fs.unlinkSync(this.filePath);
  }

  getReportId () {
    if (this.filePath == null) return Promise.reject();

    return new Promise((resolve, reject) => {
      readLastLines.read(this.filePath, 1)
        .then((reportIdRow) => {
          csvparse(reportIdRow, { delimiter: ',' }, (err, rows) => {
            if (err) return reject(err);

            const reportId = parseInt(rows[0][1], 10);

            resolve(reportId);
          });
        })
        .catch(reject);
    });
  }

  parseFile (onData) {
    if (onData == null) return Promise.resolve();
    if (this.filePath == null) return Promise.reject();

    let isHeaderRow = true;

    return new Promise((resolve, reject) => {
      fs.createReadStream(this.filePath)
        .pipe(csvparse({ delimiter: ',' }))
        .on('data', (row) => {
          if (
            !(this.ignoreHeaderRow && isHeaderRow) &&
            !(this.ignoreFooterRow && row[0] === 'report id')
          ) {
            onData(row);
          }

          if (isHeaderRow) isHeaderRow = false;
        })
        .on('error', reject)
        .on('end', resolve);
    });
  }
}

module.exports = TimeReportParser;
