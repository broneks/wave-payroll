const Router = require('express').Router;

const models = require('../../models');

const router = new Router();

router.get('/', async (req, res) => {
  let jobGroups;

  try {
    jobGroups = await models.jobGroup.findAll();
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  res.send(jobGroups);
});

router.get('/:id([0-9]+)', async (req, res) => {
  let jobGroup;

  try {
    jobGroup = await models.jobGroup.findById(req.params.id);
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  if (jobGroup == null) {
    res.status(404).send({ error: 'not found' });
    return;
  }

  res.send(jobGroup);
});

module.exports = router;
