const Router = require('express').Router;

const models = require('../../models');
const PayrollReportGenerator = require('../../services/payroll-report-generator');

const router = new Router();

router.get('/', async (req, res) => {
  const payrollReportGenerator = new PayrollReportGenerator();
  let employeeTimeReportLogs;

  try {
    employeeTimeReportLogs = await models.sequelize.query(`
      WITH sorted_time_report_logs AS (
        SELECT
          "employeeId",
          "dateLogged",
          ("hoursWorked" * "hourlyPay") as "calculatedPay"
        FROM "timeReportLogs"
        INNER JOIN "jobGroups"
          ON "timeReportLogs"."jobGroupId" = "jobGroups".id
        ORDER BY "dateLogged"
      )
      SELECT
        "employeeId",
        ARRAY_AGG(
          DISTINCT TO_CHAR("dateLogged", 'YYYY-MM')
        ) as "datesLogged",
        JSON_OBJECT_AGG("dateLogged", "calculatedPay") as "payByDateLogged"
      FROM sorted_time_report_logs
      GROUP BY "employeeId"
      ORDER BY "employeeId"
    `);
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  payrollReportGenerator.generate(employeeTimeReportLogs[0]);

  res.send(payrollReportGenerator.report);
});

module.exports = router;
