const Router = require('express').Router;
const multer = require('multer');
const moment = require('moment');

// TODO: refactor
const path = require('path');
const uploadPath = path.join(__dirname, '../../../tmp');
const uploadCSV = multer({
  storage: multer.diskStorage({
    destination (req, file, cb) {
      cb(null, uploadPath);
    },
    filename (req, file, cb) {
      const [ originalname, extension ] = file.originalname.split('.');
      cb(null, `${file.fieldname}-${Date.now()}.${extension}`);
    }
  }),

  fileFilter (req, file, cb)  {
    if (file.mimetype !== 'text/csv') {
      return cb(null, false);
    }

    cb(null, true);
  }
});

const models = require('../../models');
const Op = models.Sequelize.Op;

const TimeReportParser = require('../../services/time-report-parser');

const router = new Router();

router.get('/', async (req, res) => {
  let timeReports;

  const where = {};
  const query = req.query;

  if (query.reportId != null) {
    where.reportId = { [Op.eq]: query.reportId };
  }

  try {
    timeReports = await models.timeReport.findAll({
      where,
    });
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  res.send(timeReports);
});

router.get('/:id([0-9]+)', async (req, res) => {
  let timeReport;

  try {
    timeReport = await models.timeReport.findById(req.params.id);
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  if (timeReport == null) {
    return res.status(404).send({ error: 'not found' });
  }

  res.send(timeReport);
});

router.post('/upload', uploadCSV.single('timeReport'), async (req, res) => {
  if (req.file == null) {
    return res.status(422).send({ error: 'a csv file is required' });
  }

  const timeReportParser = new TimeReportParser({
    filePath: req.file.path,
  });

  // parse file for report id in footer
  const reportId = await timeReportParser.getReportId();
  let timeReport;

  // create time report
  try {
    timeReport = await models.timeReport.create({ reportId });
  } catch (err) {
    timeReportParser.flushFile();

    if (err.errors && err.errors[0].type === 'unique violation') {
      return res.status(422).json({ error: `a report with id ${ reportId } has already been uploaded and cannot be re-uploaded` });
    }
    return res.status(500).json({ error: 'internal server error' });
  }

  // create time report logs
  try {
    await timeReportParser.parseFile(async (row) => {
      const [dateLogged, hoursWorked, employeeId, jobGroupTitle] = row;
      let jobGroup;

      try {
        jobGroup = await models.jobGroup.findOne({
          attributes: ['id'],
          where: {
            title: {
              [Op.eq]: jobGroupTitle,
            },
          },
        });
      } catch (err) {
        timeReportParser.flushFile();

        return res.status(500).json({ error: 'internal server error' });
      }

      models.timeReportLog.create({
        hoursWorked,
        employeeId,
        timeReportId: timeReport.dataValues.id,
        jobGroupId: jobGroup.dataValues.id,
        dateLogged: new Date(moment(dateLogged, 'D/M/YYYY')),
      }).catch((err) => {
        throw new Error(err);
      });
    });
  } catch (err) {
    timeReportParser.flushFile();

    return res.status(500).json({ error: 'internal server error' });
  } finally {
    timeReportParser.flushFile();
  }

  res.status(200).send('ok');
});

module.exports = router;
