const Router = require('express').Router;

const models = require('../../models');

const router = new Router();

router.get('/', async (req, res) => {
  let timeReportLogs;

  const where = {};
  if (req.query.employeeId) {
    where.employeeId = { [models.Sequelize.Op.eq]: req.query.employeeId.toString() };
  }

  try {
    timeReportLogs = await models.timeReportLog.findAll({
      where,
    });
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  res.send(timeReportLogs);
});

router.get('/:id([0-9]+)', async (req, res) => {

  let timeReportLog;

  try {
    timeReportLog = await models.timeReportLog.findById(req.params.id);
  } catch (err) {
    return res.status(500).json({ error: 'internal server error' });
  }

  if (timeReportLog == null) {
    res.status(404).send({ error: 'not found' });
    return;
  }

  res.send(timeReportLog);
});

module.exports = router;
