const Router = require('express').Router;

const router = new Router();

router.get('/', (req, res) => {
  res.send({ status: 'ok' });
});

module.exports = router;
