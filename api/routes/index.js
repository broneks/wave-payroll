const base = require('./base');

const timeReports = require('./v0/time-reports');
const timeReportLogs = require('./v0/time-report-logs');
const jobGroups = require('./v0/job-groups');
const payrollReport = require('./v0/payroll-report');

const prepend = (path, prefix = '') => `${prefix}${path}`;
const v0 = path => prepend(path, '/v0');

module.exports = (app) => {
  app.use('/', base);

  app.use(v0('/time-reports'), timeReports);
  app.use(v0('/time-report-logs'), timeReportLogs);
  app.use(v0('/job-groups'), jobGroups);
  app.use(v0('/payroll-report'), payrollReport);
};
